Cesium.Camera.DEFAULT_VIEW_FACTOR = 0;

var cesiumWidget = new Cesium.Viewer('cesiumContainer');

var heatInstance = CesiumHeatmap.create(cesiumWidget, {
    west: getMapBounds()[0][1],
    south: getMapBounds()[1][0],
    east: getMapBounds()[1][1],
    north: getMapBounds()[0][0]
}, {
    useEntitiesIfAvailable: false,
    minOpacity: .1,
    maxOpacity: .5,
    radius: 10
});

function getDistance() {
    let cameraPosition = cesiumWidget.scene.camera.positionWC;
    let ellipsoidPosition = cesiumWidget.scene.globe.ellipsoid.scaleToGeodeticSurface(cameraPosition);
    return Cesium.Cartesian3.magnitude(Cesium.Cartesian3.subtract(cameraPosition, ellipsoidPosition, new Cesium.Cartesian3()));
};

function getZoom() {
    // This gets the cesium camera height and assigns a 2d zoom value
    // this is a little wonky and should be tweaked
    // TODO: add expansion level
    let distance = getDistance();

    // zoom levels came from comparing zoom heights at two places with a 2d map with the same extent
    // radius determined by 'what looked best', and is, i think, in screen pixels
    try {
        let zoomLevel = cesiumWidget.scene.globe._surface.tileProvider._tilesToRenderByTextureCount[cesiumWidget.scene.globe._surface.tileProvider._tilesToRenderByTextureCount.length - 1][0]._level;
        let radiusLevel = zoomLevel * 2;
        if (zoomLevel == 4) {
            radiusLevel = zoomLevel * 2;
        }
        if (zoomLevel < 4) {
            zoomLevel = 4;
        }
        return {
            zoom: zoomLevel,
            radius: radiusLevel
        };
    } catch (err) {
        let longestArray = []
        for (let a in cesiumWidget.scene.globe._surface.tileProvider._tilesToRenderByTextureCount) {
            if (cesiumWidget.scene.globe._surface.tileProvider._tilesToRenderByTextureCount[a].length > longestArray.length) {
                longestArray = cesiumWidget.scene.globe._surface.tileProvider._tilesToRenderByTextureCount[a]
            }
        }

        try {
            let zoomLevel = longestArray[0]._level;
            if (zoomLevel < 4) {
                zoomLevel = 4;
            }
            let radiusLevel = zoomLevel * 2;
            return {
                zoom: zoomLevel,
                radius: radiusLevel
            };
        } catch (err) {
            return {
                zoom: 4,
                radius: 10
            }
        }
    }
}

// returns the boundaries,
// format: [[north, west],[south, east]]
function getMapBounds() {

    // maximum bounds
    let north = cesiumWidget.camera.computeViewRectangle().north * 180 / Math.PI
    let east = cesiumWidget.camera.computeViewRectangle().east * 180 / Math.PI
    let south = cesiumWidget.camera.computeViewRectangle().south * 180 / Math.PI
    let west = cesiumWidget.camera.computeViewRectangle().west * 180 / Math.PI

    let bounds = [];
    if (east > 171) {
        east = 171
    }
    if (west < -171) {
        west = -171
    }
    // north/west maximums
    bounds.push([
        north,
        west
    ]);
    // south/east maximums
    bounds.push([
        south,
        east
    ]);
    return bounds;
}

function retrieveData() {
    /////////////
    let dataObject = []
    let bounds = this.getMapBounds();
    let boundsString = bounds[0][0] + "," + bounds[0][1] + "," + bounds[1][0] + "," + bounds[1][1];
    for (var a in data) {
        if (
            (data[a].x <= bounds[1][1]) && // x <= east
            (data[a].x >= bounds[0][1]) && // x >= west
            (data[a].y >= bounds[1][0]) && // y >= west
            (data[a].y <= bounds[0][0]) //
        ) {
            dataObject.push(data[a])
        }
    }
    console.log(1, _.max(_.map(dataObject, 'value')), dataObject)
    heatInstance.setWGS84Data(1, _.max(_.map(dataObject, 'value')), dataObject);
    heatInstance.updateLayer()
        /////////////
}
retrieveData();
cesiumWidget.camera.moveEnd.addEventListener(function() {
    // the camera stopped moving
    retrieveData();
    // heatInstance.updateLayer()
});
